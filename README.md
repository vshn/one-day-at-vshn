# One Day at VSHN - The DevOps Company

If you were visiting VSHN to experience what we are doing, or sort of, your day may look like this.

## Virtual Machines

When we manage services we don't install physical machines. Instead, we install
"virtual machines". This is like installing a server (a "host") like a program
inside your computer and boot the server like starting an application, in a window.

_Let's try this with tools that we use every day:_

- VirtualBox
- [Windows 10 ISO download](https://www.microsoft.com/en-in/software-download/windows10ISO)
- Vagrant
- [Vagrant boxes](https://app.vagrantup.com/boxes/search) -- *try "Microsoft" and "Unity"*

## Programming

When our customers ask us for help it is very often about applications that run
on the Web (i.e. websites, online shops, and similar things). These applications
are written in markup and programming languages.

_Let's take a look what developing software like that feels like:_

- Python
- HTML, CSS, JavaScript
- [Introduction to programming](https://slides.com/bittner/programming-and-robotics/#/)
- [PythonTurtle](http://pythonturtle.org/)

## GNU/Linux

Most work that we do is on operating systems of the Linux family. And a large
part of our daily work we do in text-based terminals, typing commands the
computer then executes.

_Here are some resources that may give you an idea about that type of work:_

- [Introduction to Linux Command Shell for Beginners](docs/linux-shell-introduction.pdf) (PDF)
- [Ubuntu bootable USB stick](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-windows)
- [Roadmaps to becoming a developer](https://roadmap.sh/)
